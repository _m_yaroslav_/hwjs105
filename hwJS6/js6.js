function createNewUser() {
    const newUser = {
        firstName: '',
        lastName: '',
        birthday: '',
        getAge() {

            newUser.birthday = prompt('Введіть дату народження у форматі dd.mm.yyyy:', '');
            const dateYear = this.birthday.substring(6, 10)
            const dateMonth = this.birthday.substring(3, 5)
            const dateDay = this.birthday.substring(0, 2)
            const fullData = new Date(dateYear, dateMonth - 1, dateDay)
            const today = new Date();

            let age = today.getFullYear() - fullData.getFullYear();
            const month = today.getMonth() - fullData.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < fullData.getDate())) {
                age--;
            }


            return age


        }


    }
    newUser.getPassword = function (){
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + newUser.birthday.substring(6, 10);

    }


    newUser.getLogin = function () {
        return (this.firstName[0] + this.lastName ).toLowerCase() // this вказує на поточний об'єкт, у контексті якого виконується цей код.
    }


    newUser.firstName = prompt('Введіть ім\'я:', '');
    newUser.lastName = prompt('Введіть прізвище:', '');
    return newUser;
}

const user = createNewUser()
// user.getAge()
console.log(user.getAge(), user.getPassword())