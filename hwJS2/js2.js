let name = prompt("Enter your name:");
let age = prompt("Enter your age:");

//перевірка на ведення ім'я та віку:
if (!name || isNaN(age)) {
alert("Будь ласка, введіть коректні дані.");
} else {
    age = parseInt(age); //перетворення віку на число
}

if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert("Welcome, " + name);
    } else {
        alert("You are not allowed to visit this website.");
    }
} else {
    alert("Welcome, " + name);
}

/*
Теоретичні питання

1 Які існують типи даних у Javascript?
2 У чому різниця між == і ===?
3 Що таке оператор?

1. Числа (Numbers), Строки (Strings), Булевы значения (Booleans), Null, Undefined,
Объекты (Objects), Массивы (Arrays), Функции (Functions)

2.Оператор == называется оператором равенства и сравнивает значения двух операндов без учета их типа данных.
 Если значения операндов равны, то оператор возвращает true, в противном случае - false.

 Оператор === называется оператором строгого равенства и сравнивает значения двух операндов с учетом их типа данных.
  Если значения операндов и их типы данных совпадают, то оператор возвращает true, в противном случае - false.

3.  Это символ или набор символов, используемый для выполнения операции с одним или несколькими операндами.
 Операторы используются для создания выражений, которые представляют собой комбинацию операторов и операндов,
  и позволяют выполнять различные действия в программе.

JavaScript поддерживает множество операторов различных типов,
 включая арифметические, логические, сравнения, присваивания, условные и другие.
 */




